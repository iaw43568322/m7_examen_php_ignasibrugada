<?php
include_once('_header.php');
include('Class/ListaClass.php');

// Guardamos la asignatura seleccionada
$subject = $_GET["subject"];
// Creamos un objeto ListaClass
$listaClass = new Lista($subject);
// var_dump($listaClass->get());
$list = $listaClass->get();

?>
<div class="row">
    <table class="table">
        <tr>
            <th>Title</th>
            <th>Documento</th>
        </tr>
        <?php
            foreach($list as $apunte) {
        ?>
        <tr>
            <td><?= $apunte->title(); ?></td>
            <td>
                <a href="<?= $apunte->filename(); ?>"><button class="btn btn-primary">Proba</button></a>
            </td>
        </tr>
        <?php
            }
        ?>
    </table>
</div>
<?php include_once('_footer.php') ?>